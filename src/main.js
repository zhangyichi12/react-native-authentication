import React, {
    View,
    Text,
    StyleSheet,
    Component
} from 'react-native';

export default class Main extends Component {
    render() {
        return (
            <View style={ styles.container }>
                <Text>I am both iOS and Android.</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
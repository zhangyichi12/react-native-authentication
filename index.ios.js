/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, { AppRegistry } from 'react-native';
import Main from './src/main.js';

AppRegistry.registerComponent('authentication', () => Main);
